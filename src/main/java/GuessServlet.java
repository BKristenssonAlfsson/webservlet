import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/")
public class GuessServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Inject
    private GuessService guessService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        int number;
        String message;

        response.setContentType("text/plain");

        number = Integer.parseInt(request.getParameter("number"));

        message = guessService.guess(number);

        response.getWriter().println(message);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
       int number;
       String message;

       response.setContentType("text/plain");

       number = Integer.parseInt(request.getParameter("number"));

       message = guessService.guess(number);

       response.getWriter().println(message);
    }
}
